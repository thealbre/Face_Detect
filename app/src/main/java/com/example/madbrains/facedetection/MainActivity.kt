package com.example.madbrains.facedetection

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.facedetectionmodule.PhotoViewer
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		val photoViewer = PhotoViewer()
		val photoPath = R.raw.face2

		textView.text = "Face on photo: " + photoViewer.getFaceCount(applicationContext, photoPath).toString()

	}
}
