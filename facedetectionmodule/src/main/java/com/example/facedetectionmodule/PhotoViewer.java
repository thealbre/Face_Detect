package com.example.facedetectionmodule;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import java.io.InputStream;


public class PhotoViewer {
	private static final String TAG = "PhotoViewerActivity";

	public int getFaceCount(Context context, int photoPath) {

		InputStream stream = context.getResources().openRawResource(photoPath);
		Bitmap bitmap = BitmapFactory.decodeStream(stream);

		FaceDetector detector = new FaceDetector.Builder(context.getApplicationContext())
				.setTrackingEnabled(false)
				.setLandmarkType(FaceDetector.ALL_LANDMARKS)
				.build();

		Detector<Face> safeDetector = new SafeFaceDetector(detector);

		Frame frame = new Frame.Builder().setBitmap(bitmap).build();
		SparseArray<Face> faces = safeDetector.detect(frame);

		if (!safeDetector.isOperational()) {
			// Note: The first time that an app using face API is installed on a device, GMS will
			// download a native library to the device in order to do detection.  Usually this
			// completes before the app is run for the first time.  But if that download has not yet
			// completed, then the above call will not detect any faces.
			//
			// isOperational() can be used to check if the required native library is currently
			// available.  The detector will automatically become operational once the library
			// download completes on device.
			Log.w(TAG, "Face detector dependencies are not yet available.");

			IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
			boolean hasLowStorage = context.registerReceiver(null, lowstorageFilter) != null;

			if (hasLowStorage) {
				Toast.makeText(context, R.string.low_storage_error, Toast.LENGTH_LONG).show();
				Log.w(TAG, context.getString(R.string.low_storage_error));
			}
		}

		// Although detector may be used multiple times for different images, it should be released
		// when it is no longer needed in order to free native resources.
		safeDetector.release();

		return faces.size();
	}
}
